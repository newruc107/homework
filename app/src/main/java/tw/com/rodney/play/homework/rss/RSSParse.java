package tw.com.rodney.play.homework.rss;

/**
 * Created by rodeny on 2017/11/23.
 */


        import android.content.Context;
import android.net.http.AndroidHttpClient;
import android.util.Log;
import android.util.Xml;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;



public class RSSParse {
    public static String getRss(Context context, String url)
            throws IOException{

        StringBuilder sb = new StringBuilder();

        AndroidHttpClient client = AndroidHttpClient.newInstance("TEST");
        HttpGet get = new HttpGet(url);

        try{
            HttpResponse response = client.execute(get);

            BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            String line;
            while((line = br.readLine()) != null){
                sb.append(line);
            }

        }finally {
            client.close();
        }

        String result = sb.toString();
        Log.i("RSS解析後原型",result);
        return result;

    }

    public static List<RssItem> parse(String xml){

        List<RssItem> list = new ArrayList<RssItem>();

        XmlPullParser xmlPullParser = Xml.newPullParser();

        try {
            xmlPullParser.setInput(new StringReader(xml));
        } catch (XmlPullParserException e) {
            Log.d("XML解析失敗", e.getMessage());
        }

        try {
            int eventType;
            String data;
            int itemFlg = -1;
            String fieldName = null;
            RssItem item = new RssItem();

            eventType = xmlPullParser.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                if(eventType == XmlPullParser.START_DOCUMENT) {
                    Log.d("RSS解析", "START_DOCUMENT");

                } else if(eventType == XmlPullParser.END_DOCUMENT) {
                    Log.d("RSS解析", "END_DOCUMENT");

                } else if(eventType == XmlPullParser.START_TAG) {
                    data = xmlPullParser.getName();
                    Log.d("RSS解析", "Start tag "+ data);
                    if(data.equals("item")){
                        itemFlg = 1;
                        item = new RssItem();
                    }
                    fieldName = data;

                } else if(eventType == XmlPullParser.END_TAG) {
                    data = xmlPullParser.getName();
                    Log.d("RSS解析", "End tag "+ data);
                    if(data.equals("item")){
                        itemFlg = 0;
                        list.add(item);
                    }

                } else if(eventType == XmlPullParser.TEXT) {
                    data = xmlPullParser.getText();
                    Log.e("RSS解析：Text", data);

                    if(itemFlg == 1){
                        if(fieldName.equals("title")){
                            Log.d("RSS解析：title", data);
                            item.setTitle(data);
                            fieldName = "";
                        }
                        if(fieldName.equals("description")){
                            Log.d("RSS解析：description", data);
                            item.setDescription(data);
                            fieldName = "";
                        }
                        if(fieldName.equals("link")){
                            Log.d("RSS解析：link", data);
                            item.setLink(data);
                            fieldName = "";
                        }
                    }

                }
                eventType = xmlPullParser.next();
            }

        } catch (Exception e) {
            Log.d("Parse 失敗", e.getMessage());
        }

        return list;

    }
}
