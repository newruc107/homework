package tw.com.rodney.play.homework.fragment;

import android.Manifest;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.List;

import tw.com.rodney.play.homework.Album.AlbumItemViewType;
import tw.com.rodney.play.homework.Album.AlbumRecyclerViewAdapter;
import tw.com.rodney.play.homework.Album.SavePhotoTask;
import tw.com.rodney.play.homework.MainActivity;
import tw.com.rodney.play.homework.R;
import tw.com.rodney.play.homework.Tools;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnAlbumFragmentInteractionListener}
 * interface.
 */
public class AlbumFragment extends Fragment implements SavePhotoTask.SavePhotoCallback{


    private int mColumnCount = 3;

    private RecyclerView mRecyclerView;

    private OnAlbumFragmentInteractionListener mListener;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public AlbumFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_album_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
             mRecyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                mRecyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                mRecyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
            //TODO

            mRecyclerView.setAdapter(new AlbumRecyclerViewAdapter(getALbumItems(), mListener));
        }
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        String[] permissions = {Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.CAMERA};

        if (!Tools.isPermissionsGranted(getActivity(),permissions)){
            requestPermissions(permissions,MainActivity.ActionRequestCode.CAMERA.ordinal());
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnAlbumFragmentInteractionListener) {
            mListener = (OnAlbumFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnAlbumFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onSaveComplete(File file) {
        ((AlbumRecyclerViewAdapter)mRecyclerView.getAdapter()).addPhoto(file);

    }


    private List<File> getALbumItems(){
        List<File> files = new ArrayList<>();
        files.add(new File(""));
        FilenameFilter namefilter =new FilenameFilter(){
            private String[] filter={
                    "jpg","png"
            };
            @Override
            public boolean accept(File dir, String filename){
                for(int i=0;i<filter.length;i++){
                    if(filename.indexOf(filter[i])!=-1)
                        return true;
                }
                return false;
            }
        };
        try{
            File filePath=new File(getActivity().getFilesDir()+"/product");
            for(File file:filePath.listFiles(namefilter))
                files.add(file);


        }catch(Exception e){
            e.printStackTrace();
        }
        return files;
    }

    public interface OnAlbumFragmentInteractionListener {
        void onAlbumItemClicked(AlbumItemViewType viewType, File file);
    }
}
