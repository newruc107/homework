package tw.com.rodney.play.homework.Album;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.AsyncTask;
import android.util.Log;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import static android.content.ContentValues.TAG;

/**
 * Created by rodeny on 2017/11/25.
 */

public class SavePhotoTask extends AsyncTask<Bitmap, Void, File>{



    private final Context mContext;
    private final SavePhotoCallback mCallback;

    public SavePhotoTask(Context context, SavePhotoCallback callback){
        this.mContext = context;
        this.mCallback = callback;
    }

    @Override
    protected File doInBackground(Bitmap... bitmaps) {
        Bitmap image = bitmaps[0];
        return saveBitmapToFile(mContext,combineBitmap(image));

//        return saveBitmapToFile(mContext, image);
    }

    @Override
    protected void onPostExecute(File file) {
        super.onPostExecute(file);
        mCallback.onSaveComplete(file);
    }

    public interface SavePhotoCallback {
        void onSaveComplete (File filePath);
    }

    public  Bitmap combineBitmap(Bitmap bitmap){
        int width = 1000, height = 1000;
        Bitmap newBitmap = null;

        try {
            Bitmap frameBitmap = Picasso.with(mContext)
                                        .load("file:///android_asset/drawable/frame_circle.png")
                                        .resize(width, height)
                                        .get();
             newBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

            Canvas comboImage = new Canvas(newBitmap);

            comboImage.drawBitmap(bitmap, 0f, 0f, null);
            comboImage.drawBitmap(frameBitmap, 0f, 0f, null);



//            Looper.prepare();
//                    Handler handler = new Handler(Looper.myLooper());
//                    handler.post(new Runnable() {
//
//                        @Override
//                        public void run() {
//
//                            imageView.setImageBitmap(finalBitmap);
//                            Looper.loop();
//                        }
//                    });


        } catch (IOException e) {
            e.printStackTrace();
        }
        return newBitmap;
    }

    public static File saveBitmapToFile(Context context, Bitmap bitmap){


        String photoPath = context.getFilesDir() + "/product/" + System.currentTimeMillis() + ".jpg";

        File outputFile = new File(photoPath);
        if (!outputFile.getParentFile()
                       .exists()) outputFile.getParentFile().mkdir();



        try {
            FileOutputStream fos = new FileOutputStream(outputFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, fos);
            fos.close();



        } catch (FileNotFoundException e) {
            Log.d(TAG, "File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.d(TAG, "Error accessing file: " + e.getMessage());
        }
        return outputFile;

    }

}
