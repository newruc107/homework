package tw.com.rodney.play.homework.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import tw.com.rodney.play.homework.R;
import tw.com.rodney.play.homework.rss.RSSParse;
import tw.com.rodney.play.homework.rss.RSSRecyclerViewAdapter;
import tw.com.rodney.play.homework.rss.RssItem;


public class RSSListFragment extends Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private OnRSSListFragmentInteractionListener mListener;
    private List<RssItem> mRsslist;

    private RecyclerView mRecyclerView;
    private Handler mHandler;
    private final String RSS_URL = "http://news.ltn.com.tw/rss/focus.xml";
    private String RssText;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public RSSListFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static RSSListFragment newInstance(int columnCount) {
        RSSListFragment fragment = new RSSListFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }

        mHandler = new Handler();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_rss_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            mRecyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                mRecyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                mRecyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
            getRssList();
//            if(mRsslist == null) mRsslist = new ArrayList<>();
//            mRecyclerView.setAdapter(new RSSRecyclerViewAdapter(mRsslist, mListener));


        }
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
                mListener.showProgressBar(true);

    }

    private void getRssList(){
        Thread thread1 = new Thread() {
            @Override
            public void run() {
                try {
                    // URLよりRSSを取得
                    RssText = RSSParse.getRss(getActivity(), RSS_URL);
                    mRsslist = RSSParse.parse(RssText);

                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            mRecyclerView.setAdapter(new RSSRecyclerViewAdapter(mRsslist, mListener));
                            mListener.showProgressBar(false);

                        }
                    });

                } catch (Exception e) {
                    Log.d("onActivityCreated", e.getMessage());
                }
            }
        };
        thread1.start();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnRSSListFragmentInteractionListener) {
            mListener = (OnRSSListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnRSSListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }




    public interface OnRSSListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onRSSListFragmentInteraction(RssItem item);
        void showProgressBar(boolean show);
    }
}
