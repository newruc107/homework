package tw.com.rodney.play.homework.Album;

/**
 * Created by rodeny on 2017/11/24.
 */

public enum AlbumItemViewType {
    Camera,
    Photo
}
