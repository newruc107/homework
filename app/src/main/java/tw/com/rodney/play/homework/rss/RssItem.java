package tw.com.rodney.play.homework.rss;

/**
 * Created by rodeny on 2017/11/23.
 */

public class RssItem {

    private String title;
    private String description;
    private String link;

    public String getTitle() {
        return title;
    }

    public String getLink() {
        return link;
    }

    public String getDescription() {
        return description;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
