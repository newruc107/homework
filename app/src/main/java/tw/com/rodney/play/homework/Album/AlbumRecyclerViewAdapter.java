package tw.com.rodney.play.homework.Album;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import tw.com.rodney.play.homework.R;
import tw.com.rodney.play.homework.fragment.AlbumFragment.OnAlbumFragmentInteractionListener;


public class AlbumRecyclerViewAdapter extends RecyclerView.Adapter<AlbumRecyclerViewAdapter.ViewHolder> {


    private List<File> mValues;
    private final OnAlbumFragmentInteractionListener mListener;

    public AlbumRecyclerViewAdapter(List<File> items, OnAlbumFragmentInteractionListener listener) {
        mValues = items;
        if (mValues == null) {
            mValues = new ArrayList<>();
            for(int i=0;i<20;i++) mValues.add(new File(""));
        }
        mListener = listener;
    }

    @Override
    public int getItemViewType(int position) {

        return position == 0 ? AlbumItemViewType.Camera.ordinal() : AlbumItemViewType.Photo.ordinal();

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                                  .inflate(R.layout.fragment_album_item_photo, parent, false);
        switch (AlbumItemViewType.values()[viewType]){
            case Camera:
                return new ViewHolder(view, AlbumItemViewType.Camera);
            case Photo:
                return new ViewHolder(view, AlbumItemViewType.Photo);
            default:
                return new ViewHolder(view, AlbumItemViewType.Photo);

        }

    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.uri = mValues.get(position);

        switch (holder.mType) {
            case Camera:
                Picasso.with(holder.mView.getContext())
                       .load("file:///android_asset/drawable/btn_camera.png")
                       .into(holder.mPhoto);
                break;
            case Photo:
                Picasso.with(holder.mView.getContext())
                       .load(holder.uri)
                       .resize(200,200)
                       .into(holder.mPhoto);
                break;
        }


        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    mListener.onAlbumItemClicked(holder.mType, holder.uri);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }


    public void addPhoto(File file) {
        mValues.add(file);
        notifyItemInserted(mValues.size()-1);
    }



    public class ViewHolder extends RecyclerView.ViewHolder {
        private AlbumItemViewType mType;
        private View mView;
        private ImageView mPhoto;
        private File uri;


        public ViewHolder(View view, AlbumItemViewType type) {
            super(view);
            mType = type;
            mView = view;
            mPhoto = view.findViewById(R.id.album_item_photo);


        }


    }
}
