package tw.com.rodney.play.homework;

import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.content.ContextCompat;

/**
 * Created by rodeny on 2017/11/24.
 */

public class Tools {

    public static boolean isPermissionGranted(Context context, String permission){
        return ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean isPermissionsGranted(Context context, String[] permissions){
        for(String s:permissions){
            if(ContextCompat.checkSelfPermission(context, s) != PackageManager.PERMISSION_GRANTED)
                return false;
        }
        return true;
    }
}
