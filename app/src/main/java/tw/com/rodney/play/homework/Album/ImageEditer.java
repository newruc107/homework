package tw.com.rodney.play.homework.Album;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.PixelFormat;
import android.graphics.drawable.Drawable;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by rodeny on 2017/11/23.
 */

public class ImageEditer {


    public ImageEditer(){

    }



    public void getBitmapInfo(Bitmap bitmap, String title){
        Log.d("圖片資訊:"+title,"寬度："+bitmap.getWidth()+
                            "高度："+bitmap.getHeight()+
                            "記憶體大小："+bitmap.getByteCount());
    }

    public Bitmap getBitmapFromAssets(Context context, String fileName){
        AssetManager assets = context.getResources().getAssets();
        InputStream buffer = null;
        try {
            buffer = new BufferedInputStream((assets.open("drawable/"+fileName+".png")));
        } catch (IOException e) {
            e.printStackTrace();
            Log.d("Bitmap", e.getMessage());
        }
        Bitmap bitmap = BitmapFactory.decodeStream(buffer);
        if (bitmap != null) Log.d("Bitmap", bitmap.getWidth() + "," + bitmap.getHeight());
        else Log.d("Bitmap", "null");
        return bitmap;
    }

    public Bitmap compressDrawable(Drawable drawable, int w, int h){


        int width = drawable.getIntrinsicWidth();   // 取 drawable 的长宽
        int height = drawable.getIntrinsicHeight();
        Log.d("原圖長寬","寬度："+drawable.getIntrinsicWidth()+"，長度："+drawable.getIntrinsicHeight());
        Bitmap.Config config = drawable.getOpacity() != PixelFormat.OPAQUE ? Bitmap.Config.ARGB_8888 : Bitmap.Config.RGB_565;         // 取 drawable 的颜色格式
        Bitmap oldBmp = Bitmap.createBitmap(width, height, config);     // 建立对应 bitmap
        Canvas canvas = new Canvas(oldBmp);         // 建立对应 bitmap 的画布
        drawable.setBounds(0, 0, width, height);
        drawable.draw(canvas);      // 把 drawable 内容画到画布中


        Matrix matrix = new Matrix();   // 创建操作图片用的 Matrix 对象
        float scaleWidth = ((float)w / width);   // 计算缩放比例
        float scaleHeight = ((float)h / height);
        matrix.postScale(scaleWidth, scaleHeight);         // 设置缩放比例
        return Bitmap.createBitmap(oldBmp, 0, 0, width, height, matrix, true);
    }


}
