package tw.com.rodney.play.homework;

import android.Manifest;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import tw.com.rodney.play.homework.Album.AlbumItemViewType;
import tw.com.rodney.play.homework.Album.SavePhotoTask;
import tw.com.rodney.play.homework.fragment.AlbumFragment;
import tw.com.rodney.play.homework.fragment.RSSListFragment;
import tw.com.rodney.play.homework.fragment.WebFragment;
import tw.com.rodney.play.homework.rss.RssItem;

@SuppressWarnings("ResultOfMethodCallIgnored")
public class MainActivity extends FragmentActivity
        implements OnMapReadyCallback, AlbumFragment.OnAlbumFragmentInteractionListener,
                   RSSListFragment.OnRSSListFragmentInteractionListener, WebFragment.OnWebFragmentInteractionListener {


    public enum ActionRequestCode {
        LOCATION,
        CAMERA,
        NETWORK,
        TEST
    }

    public enum FragmentTag {
        Map,
        Album,
        RSSList,
        Web
    }

    private GoogleMap mMap;
    private FusedLocationProviderClient mFusedLocationClient;

    private SupportMapFragment mMapFragment;
    private Fragment currentFragment;

    private ProgressBar mProgressBar;
    private ImageView imageView;

    private List<Uri> photoUri = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        mMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mMapFragment.getMapAsync(this);
        currentFragment = mMapFragment;

        imageView = findViewById(R.id.imageView);

        findViewById(R.id.main_btn_map).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchFragment(FragmentTag.Map);
            }
        });
        findViewById(R.id.main_btn_album).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchFragment(FragmentTag.Album);
            }
        });
        findViewById(R.id.main_btn_rss).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchFragment(FragmentTag.RSSList);
            }
        });
        mProgressBar = findViewById(R.id.progressBar);
        mProgressBar.setVisibility(View.GONE);


    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        getMyCurrentPosition();

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ActionRequestCode actionRequestCode = ActionRequestCode.values()[requestCode];

        switch (actionRequestCode) {
            case LOCATION:
                moveToMyPosition();
                break;
            case CAMERA:
                if (resultCode == RESULT_OK) {
                    decorationPhoto();
                }
                break;
            case TEST:
                Log.d("123", "123");
                break;

        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);


                if (Tools.isPermissionGranted(this,Manifest.permission.ACCESS_FINE_LOCATION)) {
                    getMyCurrentPosition();
                } else {
//                    handlePermissionsDeniedMessage(actionRequestCode);
                }

    }


    @Override
    public void onAlbumItemClicked(AlbumItemViewType viewType, File file) {
        switch (viewType) {
            case Camera:

                String filePath = getFilesDir() + "/images/" + System.currentTimeMillis() + ".jpg";

                File outputFile = new File(filePath);
                if (!outputFile.getParentFile()
                               .exists()) outputFile.getParentFile()
                                                    .mkdir();

                Uri contentUri = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID + ".file_provider", outputFile);
                photoUri.add(contentUri);

                if (Tools.isPermissionGranted(this, Manifest.permission.CAMERA)) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, contentUri);
                    startActivityForResult(intent, ActionRequestCode.CAMERA.ordinal());
                } else {
                    requestPermissions(new String[]{Manifest.permission.CAMERA}, ActionRequestCode.CAMERA.ordinal());
                }

                break;
            case Photo:
                // 不用寫
                break;
        }
    }

    @Override
    public void onRSSListFragmentInteraction(RssItem item) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction()
                                                                     .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);

        Bundle bundle = new Bundle();
        bundle.putString("title", item.getTitle());
        bundle.putString("link", item.getLink());

        WebFragment toFrag = new WebFragment();
        toFrag.setArguments(bundle);

        if (!toFrag.isAdded()) transaction.hide(currentFragment)
                                          .add(R.id.main_fragment_container, toFrag)
                                          .commit();
        else transaction.hide(currentFragment)
                        .replace(R.id.main_fragment_container, toFrag)
                        .commit();
        currentFragment = toFrag;

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void showProgressBar(boolean show) {

        if (show) mProgressBar.setVisibility(View.VISIBLE);
        else mProgressBar.setVisibility(View.GONE);
    }


    private void switchFragment(final FragmentTag to) {

        FragmentTransaction transaction = getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        Fragment toFrag = null;

        switch (to) {
            case Map:
                toFrag = mMapFragment;
                break;
            case Album:
                toFrag = getSupportFragmentManager().findFragmentByTag(FragmentTag.Album.name());
                if (toFrag == null) toFrag = new AlbumFragment();
                break;
            case RSSList:
                toFrag = getSupportFragmentManager().findFragmentByTag(FragmentTag.RSSList.name());
                if (toFrag == null) toFrag = new RSSListFragment();
                break;
            case Web:
                toFrag = getSupportFragmentManager().findFragmentByTag(FragmentTag.Web.name());
                if (toFrag == null) toFrag = new WebFragment();
                break;
        }


        if (currentFragment == toFrag) {
            return;
        } else if (!toFrag.isAdded()) {
            transaction.hide(currentFragment)
                       .add(R.id.main_fragment_container, toFrag, to.name())
                       .commit();
        } else {
            transaction.hide(currentFragment)
                       .show(toFrag)
                       .commit();

        }
        currentFragment = toFrag;


    }


    private void getMyCurrentPosition() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) ==
            PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);


            LocationRequest mLocationRequest = new LocationRequest();
            mLocationRequest.setInterval(10000);
            mLocationRequest.setFastestInterval(5000);
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(mLocationRequest);
            SettingsClient client = LocationServices.getSettingsClient(this);
            Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());

            task.addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
                @Override
                public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                    moveToMyPosition();
                }
            });

            task.addOnFailureListener(this, new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    int statusCode = ((ApiException) e).getStatusCode();
                    switch (statusCode) {
                        case CommonStatusCodes.RESOLUTION_REQUIRED:

                            try {

                                ResolvableApiException resolvable = (ResolvableApiException) e;
                                resolvable.startResolutionForResult(MainActivity.this,
                                                                    ActionRequestCode.LOCATION.ordinal());
                            } catch (IntentSender.SendIntentException sendEx) {
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:

                            break;
                    }
                }
            });

        } else {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                               ActionRequestCode.LOCATION.ordinal());

        }
    }

    private void moveToMyPosition() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) !=
            PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) !=
            PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mFusedLocationClient.getLastLocation()
                            .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                                @Override
                                public void onSuccess(final Location location) {
                                    if (location != null) {
                                        final LatLng myPosition = new LatLng(location.getLatitude(), location.getLongitude());
                                        CameraPosition cameraPosition = new CameraPosition.Builder().target(myPosition)
                                                                                                    .zoom(14)
                                                                                                    .build();
                                        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                                        mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
                                            @Override
                                            public void onCameraIdle() {
                                                mMap.addMarker(new MarkerOptions().position(myPosition)
                                                                                  .title("經度：" + location.getLongitude() +
                                                                                         "、緯度：" + location.getLatitude()))
                                                    .showInfoWindow();
                                            }
                                        });


                                    } else {
                                        moveToMyPosition();
                                    }
                                }
                            });
    }

    private void handlePermissionsDeniedMessage(ActionRequestCode code) {

    }

    private void decorationPhoto() {
        Target target = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                savePhoto(bitmap);
            }


            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        };
        imageView.setTag(target);
        Picasso.with(this)
               .load(photoUri.get(photoUri.size() - 1))
               .resize(1000, 1000)
               .centerCrop()
               .into(target);
    }

    private void savePhoto(Bitmap bitmap) {
        Fragment frag = getSupportFragmentManager().findFragmentByTag(FragmentTag.Album.name());

        SavePhotoTask task = new SavePhotoTask(this,(SavePhotoTask.SavePhotoCallback)frag);
        task.execute(bitmap);

    }
}
